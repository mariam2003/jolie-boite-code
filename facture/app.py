from weasyprint import HTML
from flask import Flask, render_template, send_file, request
from datetime import datetime
import os
import io

html = HTML('invoice.html')
html.write_pdf('invoice.pdf')
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    posted_data = request.get_json() or {}
    today = datetime.today().strftime("%B %-d, %Y")
    default_data = {
        'duedate': 'August 1, 2019',
        'from_addr': {
            'addr_compagny': '123 rue du soleil',
            'addr2': '75000 Paris cedex 9',
            'company_name': 'La jolie boite à code'
        },
        'invoice_number': 123,
        'items': [{
            'charge': 300.0,
            'title': 'website design'
        },
            {
                'charge': 75.0,
                'title': 'Hosting (3 months)'
            },
            {
                'charge': 10.0,
                'title': 'Domain name (1 year)'
            }
        ],
        'to_addr': {
            'company_name': 'Ange Formation',
            'person_email': 'ange.formation@gmail.com',
            'person_name': 'Ange Ménager'
            'addr_customer': '1 rue des anglais'
            'addr_city' : '35000 Rennes'  
        }
    }

    duedate = posted_data.get('duedate', default_data['duedate'])
    from_addr = posted_data.get('from_addr', default_data['from_addr'])
    to_addr = posted_data.get('to_addr', default_data['to_addr'])
    invoice_number = posted_data.get('invoice_number',
                                     default_data['invoice_number'])
    items = posted_data.get('items', default_data['items'])

    total = sum([i['charge'] for i in items])
    rendered = render_template('invoice.html',
                               date=today,
                               from_addr=from_addr,
                               to_addr=to_addr,
                               items=items,
                               total=total,
                               invoice_number=invoice_number,
                               duedate=duedate)
    html = HTML(string=rendered)
    print(rendered)
    rendered_pdf = html.write_pdf()
    return send_file(io.BytesIO(rendered_pdf), attachment_filename='invoice.pdf')


if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
app = Flask(__name__)