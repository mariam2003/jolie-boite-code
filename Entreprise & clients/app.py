from flask import Flask,render_template,request,redirect,abort
from models import db,StudentModel,EntrepriseModel
app = Flask(__name__)
 
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)
 
@app.before_first_request
def create_table():
    db.create_all()


@app.route('/entreprises/create', methods=['GET', 'POST'])
def create_entreprise():
    if request.method == 'GET':
        return render_template('entreprises/createpage2.html')

    if request.method == 'POST':
        # hobby = request.form.getlist('hobbies')
        # hobbies = ','.join(map(str, hobby))
        # hobbies=",".join(map(str, hobby))

        first_name = request.form['first_name']
        num_siret = request.form['num_siret']
        adresse_post = request.form['adresse_post']
        code_post = request.form['code_post']
        ville = request.form['ville']
        description = request.form['description']
        url = request.form['url']
        entreprise = EntrepriseModel(
            first_name=first_name,
            num_siret=num_siret,
            adresse_post=adresse_post,
            code_post=code_post,
            ville=ville,
            description=description,
            url=url
        )
        db.session.add(entreprise)
        db.session.commit()
        return redirect('/')


@app.route('/')
def list_entreprise():
    entreprise = EntrepriseModel.query.all()
    return render_template('entreprises/datalist2.html', entreprises=entreprise)


@app.route('/entreprises/<int:id>')
def show_entreprise(id):
    entreprise = EntrepriseModel.query.filter_by(id=id).first()
    if entreprise:
        return render_template('entreprises/data2.html', students=entreprise)
    return f"Employee with id ={id} Doenst exist"


@app.route('/entreprises/<int:id>/edit', methods=['GET', 'POST'])
def update_entreprise(id):
    entreprise = EntrepriseModel.query.filter_by(id=id).first()

    # hobbies = student.hobbies.split(' ')
    # print(hobbies)
    if request.method == 'POST':
        if entreprise:
            db.session.delete(entreprise)
            db.session.commit()
        #     tv = request.form['tv']
        #     if tv is None:
        #               pass

        #    # print('Form:' + str(request.form))

        #     cricket = request.form['cricket']
        #     movies = request.form['movies']
        #     hobbies = tv + ' ' +  cricket + ' ' + movies
        #     print('H' + hobbies)
        # hobby = request.form.getlist('hobbies')
        # hobbies = ','.join(map(str, hobby))
        # hobbies =  ",".join(map(str, hobby))
        first_name = request.form['first_name']
        num_siret = request.form['num_siret']
        adresse_post = request.form['adresse_post']
        code_post = request.form['code_post']
        ville = request.form['ville']
        description = request.form['description']
        url = request.form['url']

        entreprise = EntrepriseModel(
            first_name=first_name,
            num_siret=num_siret,
            adresse_post=adresse_post,
            code_post=code_post,
            ville=ville,
            description=description,
            url=url
        )
        db.session.add(entreprise)
        db.session.commit()
        return redirect('/')
        return f"Student with id = {id} Does'n it exist"

    return render_template('entreprises/update2.html', student=entreprise)


@app.route('/entreprises/<int:id>/delete', methods=['GET', 'POST'])
def delete_entreprise(id):
    entreprise = EntrepriseModel.query.filter_by(id=id).first()
    if request.method == 'POST':
        if entreprise:
            db.session.delete(entreprise)
            db.session.commit()
            return redirect('/')
        abort(404)
    # return redirect('/')
    return render_template('entreprises/delete2.html')

@app.route('/clients/')
def RetrieveList():
    students = StudentModel.query.all()
    return render_template('clients/datalist.html',students = students)

@app.route('/clients/<int:id>')
def RetrieveStudent(id):
    students = StudentModel.query.filter_by(id=id).first()
    if students:
        return render_template('clients/data.html', students = students)
    return f"Employee with id ={id} Doenst exist"


@app.route('/entreprises/<id>/create_client', methods=['GET', 'POST'])
def create():
    if request.method == 'GET':
        return render_template('clients/createpage.html')

    if request.method == 'POST':
        # hobby = request.form.getlist('hobbies')
        # hobbies = ','.join(map(str, hobby))
        # hobbies=",".join(map(str, hobby))

        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        telephon = request.form['telephon']
        poste = request.form['poste']
        ville = request.form['ville']
        statut = request.form['statut']
        students = StudentModel(
            first_name=first_name,
            last_name=last_name,
            email=email,
            telephon=telephon,
            poste=poste,
            ville=ville,
            statut=statut
        )
        db.session.add(students)
        db.session.commit()
        return redirect('/')


@app.route('/clients/<int:id>/edit',methods = ['GET','POST'])
def update(id):
    student = StudentModel.query.filter_by(id=id).first()

    #hobbies = student.hobbies.split(' ')
    # print(hobbies)
    if request.method == 'POST':
        if student:
            db.session.delete(student)
            db.session.commit()
    #     tv = request.form['tv']    
    #     if tv is None:
    #               pass

    #    # print('Form:' + str(request.form))    
      
    #     cricket = request.form['cricket']
    #     movies = request.form['movies']
    #     hobbies = tv + ' ' +  cricket + ' ' + movies
    #     print('H' + hobbies)
        #hobby = request.form.getlist('hobbies')
        #hobbies = ','.join(map(str, hobby))
        #hobbies =  ",".join(map(str, hobby)) 
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        telephon = request.form['telephon']
        poste = request.form['poste']
        ville = request.form['ville']
        statut = request.form['statut']

        student = StudentModel(
            first_name=first_name,
            last_name=last_name,
            email=email,
            telephon=telephon,
            poste=poste,
            ville = ville,
            statut=statut
        )
        db.session.add(student)
        db.session.commit()
        return redirect('/')
        return f"Student with id = {id} Does'n it exist"
 
    return render_template('clients/update.html', student = student)


@app.route('/clients/<int:id>/delete', methods=['GET','POST'])
def delete(id):
    students = StudentModel.query.filter_by(id=id).first()
    if request.method == 'POST':
        if students:
            db.session.delete(students)
            db.session.commit()
            return redirect('/')
        abort(404)
     #return redirect('/')
    return render_template('clients/delete.html')
 
app.run(host='localhost', port=5000)