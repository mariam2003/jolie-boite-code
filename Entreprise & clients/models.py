from flask_sqlalchemy import SQLAlchemy
 
db =SQLAlchemy()
 
class StudentModel(db.Model):
    __tablename__ = "students"
 
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    email = db.Column(db.String())
    telephon = db.Column(db.String())
    poste = db.Column(db.String())
    ville = db.Column(db.String(80))
    statut = db.Column(db.String())
   
 
    def __init__(self, first_name,last_name,email,telephon,poste,ville,statut):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.telephon = telephon 
        self.poste = poste
        self.ville = ville
        self.statut = statut
 
    def __repr__(self):
        return f"{self.first_name}:{self.last_name}"


class EntrepriseModel(db.Model):
    __tablename__ = "entreprise"

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String())
    num_siret = db.Column(db.String())
    adresse_post = db.Column(db.String())
    code_post = db.Column(db.String())
    ville = db.Column(db.String(80))
    description = db.Column(db.String())
    url = db.Column(db.String())

    def __init__(self, first_name, num_siret, adresse_post, code_post, ville, description, url):
        self.first_name = first_name
        self.num_siret = num_siret
        self.adresse_post = adresse_post
        self.code_post = code_post
        self.ville = ville
        self.description = description
        self.url = url

    def __repr__(self):
        return f"{self.first_name}:{self.num_siret}"